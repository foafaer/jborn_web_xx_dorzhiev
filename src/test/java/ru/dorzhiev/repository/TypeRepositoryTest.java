package ru.dorzhiev.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.dorzhiev.entity.Type;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@DataJpaTest
@RunWith(SpringRunner.class)
public class TypeRepositoryTest {

    @Autowired
    TypeRepository subj;
    @Autowired
    EntityManager entityManager;
    @Mock
    TypeRepository typeRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findTypeByType() {
        Type type = subj.findTypeByType("Transport");
        assertNotNull(type);
        assertEquals("Transport", type.getType());
    }

    @Test
    public void findAllByType() {
        List<String> list = new ArrayList<>();
        list.add("Transport");
        List<Type> types = subj.findAllByType(list);
        assertNotNull(types);
        assertEquals(1L, types.size());
    }

    @Test
    public void editTypeById() {
        typeRepository.editTypeById("Travel", 1L);
        verify(typeRepository, times(1)).editTypeById("Travel", 1L);
        doNothing().when(typeRepository).editTypeById("Travel", 1L);
    }
}