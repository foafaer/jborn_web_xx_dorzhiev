package ru.dorzhiev.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.dorzhiev.entity.User;

import javax.persistence.EntityManager;

import static org.junit.Assert.*;

@DataJpaTest
@RunWith(SpringRunner.class)
public class UserRepositoryTest {

    @Autowired
    UserRepository subj;
    @Autowired
    EntityManager entityManager;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findByEmail() {
        User user = subj.findByEmail("smokeslim@gmail.com");

        assertNotNull(user);
        assertEquals(Long.valueOf(1L), user.getId());
        assertEquals("smokeslim@gmail.com", user.getEmail());
        assertEquals("$2a$10$raW3O13N6.itW/tF1OAaUO0mc9IB5g37TAVu.ugoOIUbdYEyPqgui", user.getPassword());
    }

    @Test
    public void findUserById() {
        User user = subj.findUserById(1L);

        assertNotNull(user);
        assertEquals(Long.valueOf(1L), user.getId());
        assertEquals("smokeslim@gmail.com", user.getEmail());
        assertEquals("$2a$10$raW3O13N6.itW/tF1OAaUO0mc9IB5g37TAVu.ugoOIUbdYEyPqgui", user.getPassword());
    }
}