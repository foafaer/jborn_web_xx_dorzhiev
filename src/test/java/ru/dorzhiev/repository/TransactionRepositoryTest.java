package ru.dorzhiev.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.dorzhiev.entity.Transaction;

import javax.persistence.EntityManager;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

@DataJpaTest
@RunWith(SpringRunner.class)
public class TransactionRepositoryTest {

    @Autowired
    TransactionRepository subj;
    @Autowired
    EntityManager entityManager;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findTransactions() {
        List<Transaction> transactions = subj.findTransactions("Transport", LocalDateTime.of(2021,1,1,0,0), LocalDateTime.of(2021,5,1,0,0));
        assertNotNull(transactions);
        assertEquals(1L, transactions.size());
    }
}