package ru.dorzhiev.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.dorzhiev.entity.Account;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@DataJpaTest
@RunWith(SpringRunner.class)
public class AccountRepositoryTest {

    @Autowired
    AccountRepository subj;
    @Autowired
    EntityManager entityManager;
    @Mock
    AccountRepository accountRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findAllByUserId() {
        List<Account> accounts = subj.findAllByUserId(1L);
        assertNotNull(accounts);
        assertEquals(2L, accounts.size());
    }

    @Test
    public void findById() {
        Account account = subj.findById(1L);

        assertNotNull(account);
        assertEquals(Long.valueOf(1L), account.getId());
        assertEquals("Food", account.getAccountName());
        assertEquals(new BigDecimal(1000), account.getBalance());
    }

    @Test
    public void updateFromAccount() {
        accountRepository.updateFromAccount(new BigDecimal(100), 1L);
        verify(accountRepository, times(1)).updateFromAccount(new BigDecimal(100), 1L);
        doNothing().when(accountRepository).updateFromAccount(new BigDecimal(100), 1L);

    }

    @Test
    public void updateToAccount() {
        accountRepository.updateToAccount(new BigDecimal(100), 1L);
        verify(accountRepository, times(1)).updateToAccount(new BigDecimal(100), 1L);
        doNothing().when(accountRepository).updateToAccount(new BigDecimal(100), 1L);
    }
}