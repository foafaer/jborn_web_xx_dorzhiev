package ru.dorzhiev.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.dorzhiev.api.converter.TypeConverter;
import ru.dorzhiev.entity.Type;
import ru.dorzhiev.repository.TypeRepository;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TypeServiceTest {

    @InjectMocks
    TypeService subj;
    @Mock
    TypeRepository typeRepository;
    @Mock
    TypeConverter modelToDtoConverter;

    @Test
    public void createType_typeCreated() {
        Type type = new Type().setType("food");
        TypeDto typeDto1 = new TypeDto(1L, "food");
        when(modelToDtoConverter.convert(type)).thenReturn(typeDto1);
        TypeDto typeDto = subj.createType("food");
        assertNotNull(typeDto);
        assertEquals(typeDto1, typeDto);
        verify(modelToDtoConverter, times(1)).convert(type);
    }
}