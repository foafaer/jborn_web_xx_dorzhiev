package ru.dorzhiev.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.dorzhiev.api.converter.AccountConverter;
import ru.dorzhiev.entity.Account;
import ru.dorzhiev.entity.User;
import ru.dorzhiev.repository.AccountRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @InjectMocks
    AccountService subj;
    @Mock
    AccountRepository accountRepository;
    @Mock
    AccountConverter accountConverter;

    @Test
    public void accountDtoList_accountsFound() {
        User user = new User();
        List<Account> accountList = new ArrayList<>();
        Account account = new Account();
        account.setId(2L);
        account.setUser(user);
        account.setAccountName("rent");
        account.setBalance(BigDecimal.valueOf(1000));
        accountList.add(account);
        List<AccountDto> accountDtoList = subj.accountDtoList(1L);
        assertNotNull(accountDtoList);
    }
}