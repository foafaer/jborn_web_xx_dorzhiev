package ru.dorzhiev.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.dorzhiev.api.converter.TransactionConverter;
import ru.dorzhiev.entity.Account;
import ru.dorzhiev.entity.Transaction;
import ru.dorzhiev.repository.TransactionRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    @InjectMocks
    TransactionService subj;
    @Mock
    TransactionRepository transactionRepository;
    @Mock
    TransactionConverter transactionConverter;

    @Test
    public void transactionDtoList() {
        Account fromAccount = new Account();
        Account toAccount = new Account();
        List<Transaction> transactionModelList = new ArrayList<>();
        Transaction transaction = new Transaction();
        transaction.setId(1L);
        transaction.setSum(BigDecimal.valueOf(1000));
        transaction.setTimeAndDate(LocalDateTime.of(2021, 3, 1, 0, 0));
        transaction.setFromAccount(fromAccount);
        transaction.setToAccount(toAccount);
        transactionModelList.add(transaction);
        assertNotNull(transactionModelList);
        List<TransactionDto> transactionDtoList = subj.transactionDtoList("бензин", LocalDateTime.of(2020, 3, 1, 0, 0), LocalDateTime.of(2020, 1, 1, 0, 0));
        assertNotNull(transactionDtoList);
    }
}