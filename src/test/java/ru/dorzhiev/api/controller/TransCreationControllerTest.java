package ru.dorzhiev.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.dorzhiev.api.converter.TransCreationConverter;
import ru.dorzhiev.api.json.TransCreationRequest;
import ru.dorzhiev.api.json.TransCreationResponse;
import ru.dorzhiev.configuration.SecurityConfiguration;
import ru.dorzhiev.service.TransactionDto;
import ru.dorzhiev.service.TransactionService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.MockSecurityConfiguration;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(TransCreationController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class TransCreationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    TransactionService transactionService;

    @MockBean
    TransCreationConverter converter;

    @MockBean
    UserService userService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void createTransaction() throws Exception {
        when(userService.currentUser())
                .thenReturn(new UserDto(
                        1L,
                        "Demberel",
                        "Dorzhiev",
                        "smokeslim@gmail.com",
                        "password"));

        TransCreationRequest transCreationRequest = new TransCreationRequest(
                1L,
                2L,
                BigDecimal.valueOf(1000),
                LocalDateTime.of(2021, 5, 5, 0, 0),
                Collections.emptyList());

        TransCreationResponse transCreationResponse = new TransCreationResponse(
                1L,
                BigDecimal.valueOf(1000),
                LocalDateTime.of(2021, 5, 5, 0, 0),
                1L,
                2L);

        TransactionDto transactionDto = new TransactionDto(
                1L,
                BigDecimal.valueOf(1000),
                LocalDateTime.of(2021, 5, 5, 0, 0),
                1L,
                2L);

        when(transactionService.createTransaction(1L,
                2L,
                BigDecimal.valueOf(1000),
                LocalDateTime.of(2021, 5, 5, 0, 0),
                Collections.emptyList()))
                .thenReturn(transactionDto);
        when(converter.convert(transactionDto)).thenReturn(transCreationResponse);

        mockMvc.perform(post("/api/create_transaction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(transCreationRequest)))
                .andDo(print())
                .andExpect(content().json(objectMapper.writeValueAsString(transCreationResponse)));
    }
}