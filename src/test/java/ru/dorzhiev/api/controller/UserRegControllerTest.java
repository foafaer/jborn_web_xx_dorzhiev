package ru.dorzhiev.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.dorzhiev.api.converter.UserConverter;
import ru.dorzhiev.api.json.RegRequest;
import ru.dorzhiev.api.json.RegResponse;
import ru.dorzhiev.configuration.SecurityConfiguration;
import ru.dorzhiev.entity.User;
import ru.dorzhiev.security.UserRole;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.MockSecurityConfiguration;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(UserRegController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class UserRegControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    UserService userService;

    @SpyBean
    UserConverter converter;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void signUp() throws Exception {

        UserDto userDto = new UserDto(1L,
                "Demberel",
                "Dorzhiev",
                "smokeslim@gmail.com",
                "password");

        RegRequest request = new RegRequest(
                "Demberel",
                "Dorzhiev",
                "smokeslim@gmail.com",
                "password");

        RegResponse response = new RegResponse(
                1L,
                "Demberel",
                "Dorzhiev",
                "smokeslim@gmail.com");

        when(userService.registration(
                "Demberel",
                "Dorzhiev",
                "smokeslim@gmail.com",
                "password"))
                .thenReturn(userDto);

        User user = new User()
                .setFirstName("Demberel")
                .setLastName("Dorzhiev")
                .setEmail("smokeslim@gmail.com")
                .setPassword("password")
                .setAccounts(Collections.emptyList())
                .setRoles(Collections.singleton(UserRole.USER));

        when(converter.convert(user)).thenReturn(userDto);

        mockMvc.perform(post("/api/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andDo(print())
                .andExpect(content().json(objectMapper.writeValueAsString(response)));
    }
}