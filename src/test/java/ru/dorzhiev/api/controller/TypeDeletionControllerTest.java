package ru.dorzhiev.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.dorzhiev.api.json.TypeDeletionRequest;
import ru.dorzhiev.configuration.SecurityConfiguration;
import ru.dorzhiev.service.TypeService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.MockSecurityConfiguration;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TypeDeletionController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class TypeDeletionControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    TypeService typeService;

    @MockBean
    UserService userService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void deleteType() throws Exception {
        when(userService.currentUser())
                .thenReturn(new UserDto(
                        1L,
                        "Demberel",
                        "Dorzhiev",
                        "smokeslim@gmail.com",
                        "password"));

        TypeDeletionRequest typeDeletionRequest = new TypeDeletionRequest().setType("Transport");

        doNothing().when(typeService).deleteType("Transport");

        mockMvc.perform(post("/api/delete_type")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(typeDeletionRequest)))
                .andDo(print())
                .andExpect(status().isAccepted());
    }
}