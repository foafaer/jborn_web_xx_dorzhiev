package ru.dorzhiev.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.dorzhiev.api.json.TransListRequest;
import ru.dorzhiev.api.json.TransListResponse;
import ru.dorzhiev.configuration.SecurityConfiguration;
import ru.dorzhiev.service.TransactionService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.MockSecurityConfiguration;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(TransListController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class TransListControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    TransactionService transactionService;

    @MockBean
    UserService userService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getTransactions() throws Exception {
        when(userService.currentUser())
                .thenReturn(new UserDto(
                        1L,
                        "Demberel",
                        "Dorzhiev",
                        "smokeslim@gmail.com",
                        "password"));

        TransListRequest transListRequest = new TransListRequest(
                "Transport",
                LocalDateTime.of(2020, 5, 5, 0, 0),
                LocalDateTime.of(2021, 5, 5, 0, 0));

        TransListResponse transListResponse = new TransListResponse(Collections.emptyList());

        when(transactionService.transactionDtoList(
                "Transport",
                LocalDateTime.of(2020, 5, 5, 0, 0),
                LocalDateTime.of(2021, 5, 5, 0, 0)))
                .thenReturn(Collections.emptyList());

        mockMvc.perform(get("/api/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(transListRequest)))
                .andDo(print())
                .andExpect(content().json(objectMapper.writeValueAsString(transListResponse)));
    }
}