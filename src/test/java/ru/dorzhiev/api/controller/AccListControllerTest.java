package ru.dorzhiev.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.dorzhiev.api.json.AccListResponse;
import ru.dorzhiev.configuration.SecurityConfiguration;
import ru.dorzhiev.service.AccountDto;
import ru.dorzhiev.service.AccountService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.MockSecurityConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AccListController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class AccListControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    AccountService accountService;

    @MockBean
    UserService userService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getAccounts() throws Exception {
        when(userService.currentUser())
                .thenReturn(new UserDto(
                        1L,
                        "Demberel",
                        "Dorzhiev",
                        "smokeslim@gmail.com",
                        "password"));

        List<AccountDto> accounts = new ArrayList<>();
        when(accountService.accountDtoList(1L)).thenReturn(accounts);
        AccListResponse accListResponse = new AccListResponse(accounts);

        mockMvc.perform(get("/api/accounts"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(accListResponse)));
    }
}