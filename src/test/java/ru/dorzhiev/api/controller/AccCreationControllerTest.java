package ru.dorzhiev.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.dorzhiev.api.converter.AccCreationConverter;
import ru.dorzhiev.api.json.AccCreationRequest;
import ru.dorzhiev.api.json.AccCreationResponse;
import ru.dorzhiev.configuration.SecurityConfiguration;
import ru.dorzhiev.service.AccountDto;
import ru.dorzhiev.service.AccountService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.MockSecurityConfiguration;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(AccCreationController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class AccCreationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    AccountService accountService;

    @MockBean
    UserService userService;

    @SpyBean
    AccCreationConverter converter;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void handle() throws Exception {
        when(userService.currentUser())
                .thenReturn(new UserDto(
                        1L,
                        "Demberel",
                        "Dorzhiev",
                        "smokeslim@gmail.com",
                        "password"));

        AccountDto accountDto = new AccountDto(1L, "Food", BigDecimal.valueOf(1000), 1L);
        AccCreationRequest accCreationRequest = new AccCreationRequest("Food", BigDecimal.valueOf(1000));
        AccCreationResponse accCreationResponse = new AccCreationResponse(1L, "Food", BigDecimal.valueOf(1000), 1L);

        when(accountService.createAccount("Food", BigDecimal.valueOf(1000), 1L))
                .thenReturn(accountDto);
        when(converter.convert(accountDto))
                .thenReturn(accCreationResponse);

        mockMvc.perform(post("/api/create_account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(accCreationRequest)))
                .andDo(print())
                .andExpect(content().json(objectMapper.writeValueAsString(accCreationResponse)));
    }
}