package ru.dorzhiev.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.dorzhiev.security.CustomGrantedAuthority;
import ru.dorzhiev.security.CustomUserDetails;

import static java.util.Collections.singleton;
import static ru.dorzhiev.security.UserRole.USER;

@Configuration
public class MockSecurityConfiguration {
    @Bean
    public UserDetailsService userDetailsService() {
        return username -> new CustomUserDetails(
                1L,
                "smokeslim@gmail.com",
                "password",
                singleton(new CustomGrantedAuthority(USER)));
    }
}
