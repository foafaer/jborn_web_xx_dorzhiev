package ru.dorzhiev.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.dorzhiev.configuration.SecurityConfiguration;
import ru.dorzhiev.repository.TypeRepository;
import ru.dorzhiev.repository.UserRepository;
import ru.dorzhiev.service.TypeService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.MockSecurityConfiguration;
import ru.dorzhiev.web.form.TypeCreationForm;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(TypeCreationWebController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class TypeCreationWebControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TypeService typeService;

    @MockBean
    UserService userService;

    @MockBean
    UserRepository userRepository;

    @MockBean
    TypeRepository typeRepository;

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getCreateTypePage() throws Exception {
        when(userService.currentUser()).thenReturn(new UserDto(
                1L,
                "Demberel",
                "Dorzhiev",
                "smokeslim@gmail.com",
                "password"));
        mockMvc.perform(get("/create_type"))
                .andExpect(view().name("create-type"));
    }

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void createType() throws Exception {
        when(userService.currentUser()).thenReturn(new UserDto(
                1L,
                "Demberel",
                "Dorzhiev",
                "smokeslim@gmail.com",
                "password"));
        mockMvc.perform(post("/create_type").flashAttr("form", new TypeCreationForm().setType("Transport")))
                .andExpect(view().name("redirect:/personal-page"));
    }
}