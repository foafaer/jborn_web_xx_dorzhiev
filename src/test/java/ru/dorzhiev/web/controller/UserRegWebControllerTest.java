package ru.dorzhiev.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.dorzhiev.configuration.SecurityConfiguration;
import ru.dorzhiev.repository.UserRepository;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.MockSecurityConfiguration;
import ru.dorzhiev.web.form.RegForm;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(UserRegWebController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class UserRegWebControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @MockBean
    UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void register() throws Exception {
        mockMvc.perform(get("/registration"))
                .andExpect(status().isOk())
                .andExpect(view().name("registration"));
    }

    @Test
    public void registerUser() throws Exception {
        mockMvc.perform(post("/registration")
                .flashAttr("form", new RegForm()
                        .setFirstName("Demberel")
                        .setLastName("Dorzhiev")
                        .setEmail("smokeslim@gmail.com")
                        .setPassword("password")))
                .andExpect(view().name("registration"));
    }
}