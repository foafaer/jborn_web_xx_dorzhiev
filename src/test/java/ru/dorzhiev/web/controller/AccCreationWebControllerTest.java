package ru.dorzhiev.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.dorzhiev.configuration.SecurityConfiguration;
import ru.dorzhiev.repository.AccountRepository;
import ru.dorzhiev.repository.UserRepository;
import ru.dorzhiev.service.AccountService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.MockSecurityConfiguration;
import ru.dorzhiev.web.form.AccCreationForm;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(AccCreationWebController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class AccCreationWebControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AccountService accountService;

    @MockBean
    UserService userService;

    @MockBean
    UserRepository userRepository;

    @MockBean
    AccountRepository accountRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getCreateAccountPage() throws Exception {
        when(userService.currentUser()).thenReturn(new UserDto(
                1L,
                "Demberel",
                "Dorzhiev",
                "smokeslim@gmail.com",
                "password"));
        mockMvc.perform(get("/create_account"))
                .andExpect(view().name("create-account"));
    }

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void createAccount() throws Exception {
        when(userService.currentUser()).thenReturn(new UserDto(
                1L,
                "Demberel",
                "Dorzhiev",
                "smokeslim@gmail.com",
                "password"));
        mockMvc.perform(post("/create_account")
                .flashAttr("form", new AccCreationForm().setName("Grocery").setBalance(BigDecimal.valueOf(1000))))
                .andExpect(view().name("redirect:/personal-page"));
    }
}