package ru.dorzhiev.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.dorzhiev.configuration.SecurityConfiguration;
import ru.dorzhiev.repository.TransactionRepository;
import ru.dorzhiev.repository.UserRepository;
import ru.dorzhiev.service.TransactionService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.MockSecurityConfiguration;
import ru.dorzhiev.web.form.TransCreationForm;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(TransCreationWebController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class TransCreationWebControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TransactionService transactionService;

    @MockBean
    UserService userService;

    @MockBean
    UserRepository userRepository;

    @MockBean
    TransactionRepository transactionRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getCreateTransPage() throws Exception {
        when(userService.currentUser()).thenReturn(new UserDto(
                1L,
                "Demberel",
                "Dorzhiev",
                "smokeslim@gmail.com",
                "password"));
        mockMvc.perform(get("/create_transaction"))
                .andExpect(view().name("create-transaction"));
    }

    @Test
    @WithUserDetails(value = "smokeslim@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void createTransaction() throws Exception {
        when(userService.currentUser()).thenReturn(new UserDto(
                1L,
                "Demberel",
                "Dorzhiev",
                "smokeslim@gmail.com",
                "password"));
        mockMvc.perform(post("/create_transaction")
                .flashAttr("form", new TransCreationForm()
                        .setFromAccId(1L)
                        .setToAccId(2L)
                        .setValue(BigDecimal.valueOf(1000))
                        .setDateTime(LocalDateTime.of(2021, 5, 15, 0, 0))
                        .setTypes(Collections.emptyList())))
                .andExpect(view().name("redirect:/personal-page"));
    }
}