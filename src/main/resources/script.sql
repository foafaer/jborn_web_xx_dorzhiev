create table users (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NULL,
    email VARCHAR(255) NOT  NULL,
    password VARCHAR(255) NOT NULL
);

create table account (
    id  SERIAL PRIMARY KEY,
    account_name VARCHAR(255) NOT NULL,
    balance DOUBLE PRECISION NOT NULL,
    users_id INT REFERENCES users(id) ON DELETE CASCADE
);

create table transaction (
    id SERIAL PRIMARY KEY,
    sum DOUBLE PRECISION NOT NULL,
    time_and_date TIMESTAMP NOT NULL,
    from_account_id INT REFERENCES account(id) ON DELETE CASCADE,
    to_account_id INT REFERENCES account(id) ON DELETE CASCADE
);

create table type (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

create table transaction_to_type (
    transaction_id INT REFERENCES transaction(id) ON DELETE CASCADE,
    type_id INT REFERENCES type(id) ON DELETE CASCADE
);

insert into users (first_name, last_name, email, password) values ('Гарри', 'Поттер', 'harry@mail.com', 'harry123');

insert into account (account_name, balance, users_id) values ('наличные', '5050.5', 1);
insert into account (account_name, balance, users_id) values ('карта', '17700.9', 1);
insert into account (account_name, balance, users_id) values ('мама', '200.2', 1);
insert into account (account_name, balance, users_id) values ('жена', '6000.3', 1);

insert into transaction (sum, time_and_date, from_account_id, to_account_id) values ('500', '2021-03-09 11:25:30', 15, 16);
insert into transaction (sum, time_and_date, from_account_id, to_account_id) values ('600', '2021-03-08 17:17:28', 16, 17);
insert into transaction (sum, time_and_date, from_account_id, to_account_id) values ('300', '2021-03-09 09:10:15', 17, 18);
insert into transaction (sum, time_and_date, from_account_id, to_account_id) values ('2500', '2021-03-08 18:55:45', 18, 15);

insert into type (name) values ('продукты');
insert into type (name) values ('аптека');
insert into type (name) values ('бензин');
insert into type (name) values ('подарок');
insert into type (name) values ('маме');
insert into type (name) values ('на машину');

insert into transaction_to_type (transaction_id, type_id) values (8, 1);
insert into transaction_to_type (transaction_id, type_id) values (9, 2);
insert into transaction_to_type (transaction_id, type_id) values (10, 3);
insert into transaction_to_type (transaction_id, type_id) values (11, 4);

select
    a.account_name
from account as a where users_id = 1;

select
    t.sum,
    u.first_name
from transaction as t
         join users as u on u.id < t.from_account_id
where t.time_and_date > '2021-03-06 00:00:00';

select
    sum(balance)
from account;

update users set password = md5(password);

select md5('password');

SELECT current_database();

select
    t.*
from transaction as t
         join transaction_to_type ttt on t.id = ttt.transaction_id
         join type t2 on ttt.type_id = t2.id
where t.time_and_date between '2021-03-06 00:00:00' and '2021-03-10 00:00:00' and t2.name = 'бензин';


