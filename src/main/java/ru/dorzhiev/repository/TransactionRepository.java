package ru.dorzhiev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.dorzhiev.entity.Transaction;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    @Query("select t from Transaction t join t.types tt where tt.type = :type and t.timeAndDate > :start and t.timeAndDate < :end")
    List<Transaction> findTransactions(@Param("type") String typeName, @Param("start") LocalDateTime startDate, @Param("end") LocalDateTime endDate);
}
