package ru.dorzhiev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.dorzhiev.entity.Type;

import java.util.List;

@Repository
public interface TypeRepository extends JpaRepository<Type, Long> {
    Type findTypeByType(String name);

    @Query("select t from Type t where t.type in :types")
    List<Type> findAllByType(@Param("types") List<String> types);

    @Modifying
    @Query("update Type t set t.type = :name where t.id = :id")
    void editTypeById(@Param("name") String name, @Param("id") Long id);
}
