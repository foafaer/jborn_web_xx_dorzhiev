package ru.dorzhiev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.dorzhiev.entity.Account;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    List<Account> findAllByUserId(Long userId);

    Account findById(long id);

    @Modifying
    @Query("update Account a set a.balance = a.balance - :value where a.id = :from")
    void updateFromAccount(@Param("value") BigDecimal value, @Param("from") Long id);

    @Modifying
    @Query("update Account a set a.balance = a.balance + :value where a.id = :to")
    void updateToAccount(@Param("value") BigDecimal value, @Param("to") Long id);
}
