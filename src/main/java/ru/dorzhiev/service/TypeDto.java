package ru.dorzhiev.service;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TypeDto {
    private Long id;
    private String type;
}
