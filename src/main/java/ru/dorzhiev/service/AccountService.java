package ru.dorzhiev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.dorzhiev.entity.Account;
import ru.dorzhiev.entity.User;
import ru.dorzhiev.repository.AccountRepository;
import ru.dorzhiev.repository.UserRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;
    private final Converter<Account, AccountDto> accountConverter;
    private final UserRepository userRepository;

    public List<AccountDto> accountDtoList (Long userId) {
        List<Account> accountList = accountRepository.findAllByUserId(userId);
        return accountList.stream().map(accountConverter::convert).collect(Collectors.toList());
    }

    @Transactional
    public AccountDto createAccount(String accountName, BigDecimal balance, Long userId) {
        User user = userRepository.findUserById(userId);
        Account account = new Account()
                .setAccountName(accountName)
                .setBalance(balance)
                .setUser(user);
        accountRepository.save(account);
        return accountConverter.convert(account);
    }

    public void deleteAccount(Long id) {
        accountRepository.deleteById(id);
    }
}
