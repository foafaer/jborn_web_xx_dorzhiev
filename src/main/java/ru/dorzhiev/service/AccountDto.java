package ru.dorzhiev.service;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class AccountDto {
    private Long id;
    private String accountName;
    private BigDecimal balance;
    private Long userId;
}
