package ru.dorzhiev.service;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class TransactionDto {
    private Long id;
    private BigDecimal sum;
    private LocalDateTime timeAndDate;
    private Long fromAccountId;
    private Long toAccountId;
}
