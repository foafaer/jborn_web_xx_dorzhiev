package ru.dorzhiev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.dorzhiev.entity.Type;
import ru.dorzhiev.repository.TypeRepository;

@Service
@RequiredArgsConstructor
public class TypeService {
    private final TypeRepository typeRepository;
    private final Converter<Type, TypeDto> typeConverter;

    public TypeDto createType(String name) {
        Type type = new Type()
                .setType(name);
        typeRepository.save(type);
        return typeConverter.convert(type);
    }

    public void deleteType(String name) {
        Type type = typeRepository.findTypeByType(name);
        typeRepository.delete(type);
    }

    @Transactional
    public void editType(String name, Long id) {
        typeRepository.editTypeById(name, id);
    }
}
