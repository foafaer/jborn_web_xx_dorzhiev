package ru.dorzhiev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.dorzhiev.entity.User;
import ru.dorzhiev.repository.UserRepository;
import ru.dorzhiev.security.CustomGrantedAuthority;
import ru.dorzhiev.security.CustomUserDetails;
import ru.dorzhiev.security.UserRole;

import java.util.Collections;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final Converter<User, UserDto> userConverter;
    private final PasswordEncoder passwordEncoder;

    public UserDto registration(String firstName, String lastName, String email, String password) {
        String hash = passwordEncoder.encode(password);
        User user = new User()
                .setFirstName(firstName)
                .setLastName(lastName)
                .setEmail(email)
                .setPassword(hash)
                .setRoles(Collections.singleton(UserRole.USER));
        userRepository.save(user);
        return userConverter.convert(user);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("Can't find user with email " + email);
        }
        return new CustomUserDetails(
                user.getId(),
                user.getEmail(),
                user.getPassword(),
                user.getRoles().stream().map(CustomGrantedAuthority::new).collect(Collectors.toList())
        );
    }

    public UserDto currentUser() {
        CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userConverter.convert(userRepository.findUserById(userDetails.getId()));
    }
}
