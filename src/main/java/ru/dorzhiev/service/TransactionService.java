package ru.dorzhiev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.dorzhiev.entity.Account;
import ru.dorzhiev.entity.Transaction;
import ru.dorzhiev.entity.Type;
import ru.dorzhiev.repository.AccountRepository;
import ru.dorzhiev.repository.TransactionRepository;
import ru.dorzhiev.repository.TypeRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransactionService {
    private final TransactionRepository transactionRepository;
    private final Converter<Transaction, TransactionDto> transactionConverter;
    private final AccountRepository accountRepository;
    private final TypeRepository typeRepository;

    public List<TransactionDto> transactionDtoList (String type, LocalDateTime startDate, LocalDateTime endDate) {
        List<Transaction> transactionList = transactionRepository.findTransactions(type, startDate, endDate);
        return transactionList.stream().map(transactionConverter::convert).collect(Collectors.toList());
    }

    @Transactional
    public TransactionDto createTransaction(long fromAccount, long toAccount, BigDecimal value, LocalDateTime timeAndDate, List<String> types) {
        Account fromAcc = accountRepository.findById(fromAccount);
        Account toAcc = accountRepository.findById(toAccount);
        List<Type> typeList = typeRepository.findAllByType(types);

        accountRepository.updateFromAccount(value, fromAccount);
        accountRepository.updateToAccount(value, toAccount);

        Transaction transaction = new Transaction()
                .setFromAccount(fromAcc)
                .setToAccount(toAcc)
                .setSum(value)
                .setTimeAndDate(timeAndDate)
                .setTypes(typeList);
        transactionRepository.save(transaction);
        return transactionConverter.convert(transaction);
    }
}
