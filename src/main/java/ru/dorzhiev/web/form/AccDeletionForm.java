package ru.dorzhiev.web.form;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class AccDeletionForm {
    @NotNull
    private Long accountId;
}
