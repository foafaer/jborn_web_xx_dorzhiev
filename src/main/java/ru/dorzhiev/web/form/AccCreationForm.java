package ru.dorzhiev.web.form;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class AccCreationForm {
    @NotNull
    private String name;
    @NotNull
    private BigDecimal balance;
}
