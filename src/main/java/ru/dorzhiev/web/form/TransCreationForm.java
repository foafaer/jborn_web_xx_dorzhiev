package ru.dorzhiev.web.form;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Accessors(chain = true)
public class TransCreationForm {
    @NotNull
    private Long fromAccId;
    @NotNull
    private Long toAccId;
    @NotNull
    private BigDecimal value;
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateTime;
    @NotNull
    private List<String> types;
}
