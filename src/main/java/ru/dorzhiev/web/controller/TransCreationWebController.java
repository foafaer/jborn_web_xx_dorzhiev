package ru.dorzhiev.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.dorzhiev.service.TransactionService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.form.TransCreationForm;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class TransCreationWebController {
    private final TransactionService transactionService;
    private final UserService userService;

    @GetMapping("/create_transaction")
    public String getCreateTransPage() {
        return "create-transaction";
    }

    @PostMapping("/create_transaction")
    public String createTransaction(@ModelAttribute("form") @Valid TransCreationForm form,
                              BindingResult result,
                              Model model) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return "redirect:/login-form";
        }
        if (!result.hasErrors()) {
            transactionService.createTransaction(
                    form.getFromAccId(),
                    form.getToAccId(),
                    form.getValue(),
                    form.getDateTime(),
                    form.getTypes());
            return "redirect:/personal-page";
        }
        model.addAttribute("form", form);
        return "create-transaction";
    }
}
