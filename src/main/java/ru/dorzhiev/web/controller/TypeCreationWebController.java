package ru.dorzhiev.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.dorzhiev.service.TypeService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.form.TypeCreationForm;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class TypeCreationWebController {
    private final TypeService typeService;
    private final UserService userService;

    @GetMapping("/create_type")
    public String getCreateTypePage() {
        return "create-type";
    }

    @PostMapping("/create_type")
    public String createType(@ModelAttribute("form") @Valid TypeCreationForm form,
                             BindingResult result,
                             Model model) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return "redirect:/login-form";
        }
        if (!result.hasErrors()) {
            typeService.createType(form.getType());
            return "redirect:/personal-page";
        }
        model.addAttribute("form", form);
        return "create-type";
    }
}
