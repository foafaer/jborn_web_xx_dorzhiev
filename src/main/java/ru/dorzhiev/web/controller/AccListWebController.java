package ru.dorzhiev.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.dorzhiev.service.AccountDto;
import ru.dorzhiev.service.AccountService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

@Controller
@RequiredArgsConstructor
public class AccListWebController {
    private final AccountService accountService;
    private final UserService userService;

    @GetMapping("/get_accounts")
    public String getAccounts(Model model) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return "redirect:/login-form";
        }
        Iterable<AccountDto> accountDtoList = accountService.accountDtoList(user.getId());
        if (accountDtoList != null) {
            model.addAttribute("accountDtoList", accountDtoList);
            return "show-accounts";
        }
        return "redirect:/personal-page";
    }
}
