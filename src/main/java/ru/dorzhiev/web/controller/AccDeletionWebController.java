package ru.dorzhiev.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.dorzhiev.service.AccountService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.form.AccDeletionForm;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class AccDeletionWebController {
    private final AccountService accountService;
    private final UserService userService;

    @GetMapping("/delete_account")
    public String getDeleteAccountPage() {
        return "delete-account";
    }

    @PostMapping("/delete_account")
    public String deleteAccount(@ModelAttribute("form") @Valid AccDeletionForm form,
                                BindingResult result,
                                Model model) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return "redirect:/login-form";
        }
        if (!result.hasErrors()) {
            accountService.deleteAccount(form.getAccountId());
            return "redirect:/personal-page";
        }
        model.addAttribute("form", form);
        return "delete-account";
    }
}
