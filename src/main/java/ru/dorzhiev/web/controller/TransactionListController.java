package ru.dorzhiev.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.dorzhiev.service.TransactionDto;
import ru.dorzhiev.service.TransactionService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.form.TransListForm;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class TransactionListController {
    private final TransactionService transactionService;
    private final UserService userService;

    @GetMapping("/get_transactions")
    public String getTransactionPage() {
        return "get-transactions";
    }

    @PostMapping("/get_transactions")
    public String getTransactions(@ModelAttribute("form") @Valid TransListForm form,
                                  BindingResult result,
                                  Model model) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return "redirect:/login-form";
        }
        if (!result.hasErrors()) {
            Iterable<TransactionDto> transactionDtoList = transactionService.transactionDtoList(
                    form.getType(),
                    form.getStartTime(),
                    form.getEndTime());
            if (transactionDtoList != null) {
                model.addAttribute("transactionDtoList", transactionDtoList);
                return "show-transactions";
            }
        }
        model.addAttribute("form", form);
        return "personal-page";
    }
}
