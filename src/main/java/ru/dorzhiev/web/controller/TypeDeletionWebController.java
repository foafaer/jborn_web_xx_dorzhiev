package ru.dorzhiev.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.dorzhiev.service.TypeService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.form.TypeDeletionForm;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class TypeDeletionWebController {
    final private TypeService typeService;
    private final UserService userService;

    @GetMapping("/delete_type")
    public String getDeleteTypePage(Model model) {
        model.addAttribute("form", new TypeDeletionForm());
        return "delete-type";
    }

    @PostMapping("/delete_type")
    public String deleteType(@ModelAttribute("form") @Valid TypeDeletionForm form,
                             BindingResult result,
                             Model model) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return "redirect:/login-form";
        }
        if (!result.hasErrors()) {
            typeService.deleteType(form.getType());
            return "redirect:/personal-page";
        }
        model.addAttribute("form", form);
        return "delete-account";
    }
}
