package ru.dorzhiev.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.dorzhiev.service.AccountService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.form.AccCreationForm;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class AccCreationWebController {
    private final AccountService accountService;
    private final UserService userService;

    @GetMapping("/create_account")
    public String getCreateAccountPage() {
        return "create-account";
    }

    @PostMapping("/create_account")
    public String createAccount(@ModelAttribute("form") @Valid AccCreationForm form,
                                BindingResult result,
                                Model model) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return "redirect:/login-form";
        }
        if (!result.hasErrors()) {
            accountService.createAccount(
                    form.getName(),
                    form.getBalance(),
                    user.getId());
            return "redirect:/personal-page";
        }
        model.addAttribute("form", form);
        return "create-account";
    }
}
