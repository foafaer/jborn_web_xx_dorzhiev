package ru.dorzhiev.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

@Controller
@RequiredArgsConstructor
public class UserAuthWebController {
    private final UserService userService;

    @GetMapping("/login-form")
    public String getLoginPage() {
        return "login-page";
    }

    @GetMapping("/personal-page")
    public String personalPage(Model model) {
        UserDto user = userService.currentUser();
        model.addAttribute("name", user.getFirstName());
        return "personal-page";
    }
}
