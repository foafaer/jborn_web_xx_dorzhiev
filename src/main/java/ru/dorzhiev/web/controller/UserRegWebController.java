package ru.dorzhiev.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;
import ru.dorzhiev.web.form.RegForm;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class UserRegWebController {
    private final UserService userService;

    @GetMapping("/registration")
    public String register() {
        return "registration";
    }

    @PostMapping("/registration")
    public String registerUser(@ModelAttribute("form") @Valid RegForm form,
                               BindingResult result,
                               Model model) {
        if (!result.hasErrors()) {
            UserDto userDto = userService.registration(
                    form.getFirstName(),
                    form.getLastName(),
                    form.getEmail(),
                    form.getPassword());
            if (userDto != null) {
                return "redirect:/login-form";
            }
            result.addError(new FieldError("form", "email", "Something went wrong, please try again"));
        }
        model.addAttribute("form", form);
        return "registration";
    }
}
