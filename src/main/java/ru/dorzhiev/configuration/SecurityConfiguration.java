package ru.dorzhiev.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static ru.dorzhiev.security.UserRole.ADMIN;
import static ru.dorzhiev.security.UserRole.USER;

@Configuration
public class SecurityConfiguration {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/login-form").permitAll()
                .antMatchers("/personal-page").hasAnyRole(USER.name(), ADMIN.name())
                .antMatchers("/create_account").hasAnyRole(USER.name(), ADMIN.name())
                .antMatchers("/delete_account").hasAnyRole(USER.name(), ADMIN.name())
                .antMatchers("/get_accounts").hasAnyRole(USER.name(), ADMIN.name())
                .antMatchers("/get_transactions").hasAnyRole(USER.name(), ADMIN.name())
                .antMatchers("/create_transaction").hasAnyRole(USER.name(), ADMIN.name())
                .antMatchers("/create_type").hasAnyRole(USER.name(), ADMIN.name())
                .antMatchers("/delete_type").hasAnyRole(USER.name(), ADMIN.name())
                .antMatchers("/edit_type").hasAnyRole(USER.name(), ADMIN.name())
                .and()
                .formLogin()
                .usernameParameter("email")
                .passwordParameter("password")
                .loginPage("/login-form")
                .loginProcessingUrl("/login")
                .defaultSuccessUrl("/personal-page")
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login-form")
                .and()
                .httpBasic();
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
