package ru.dorzhiev.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.dorzhiev.api.converter.TransCreationConverter;
import ru.dorzhiev.api.json.TransCreationRequest;
import ru.dorzhiev.api.json.TransCreationResponse;
import ru.dorzhiev.service.TransactionDto;
import ru.dorzhiev.service.TransactionService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TransCreationController {
    private final TransactionService transactionService;
    private final TransCreationConverter converter;
    private final UserService userService;

    @PostMapping("/create_transaction")
    public ResponseEntity<TransCreationResponse> createTransaction(@RequestBody @Valid TransCreationRequest request) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return status(HttpStatus.UNAUTHORIZED).build();
        }
        TransactionDto transactionDto = transactionService.createTransaction(
                request.getFromAccId(),
                request.getToAccId(),
                request.getValue(),
                request.getDateTime(),
                request.getTypes());
        if (transactionDto != null) {
            return ok(converter.convert(transactionDto));
        }
        return status(HttpStatus.UNAUTHORIZED).build();
    }
}
