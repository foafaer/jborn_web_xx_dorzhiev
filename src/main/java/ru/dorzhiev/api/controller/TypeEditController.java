package ru.dorzhiev.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.dorzhiev.api.json.TypeEditRequest;
import ru.dorzhiev.api.json.TypeEditResponse;
import ru.dorzhiev.service.TypeService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TypeEditController {
    private final TypeService typeService;
    private final UserService userService;

    @PostMapping("/edit_type")
    public ResponseEntity<TypeEditResponse> editType(@RequestBody @Valid TypeEditRequest request) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return status(HttpStatus.UNAUTHORIZED).build();
        }
        typeService.editType(request.getNewName(), request.getId());
        return status(HttpStatus.ACCEPTED).build();
    }
}
