package ru.dorzhiev.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.dorzhiev.api.json.TypeCreationRequest;
import ru.dorzhiev.api.json.TypeCreationResponse;
import ru.dorzhiev.service.TypeDto;
import ru.dorzhiev.service.TypeService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TypeCreationController {
    private final TypeService typeService;
    private final UserService userService;

    @PostMapping("/create_type")
    public ResponseEntity<TypeCreationResponse> createType(@RequestBody @Valid TypeCreationRequest request) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return status(HttpStatus.UNAUTHORIZED).build();
        }
        TypeDto typeDto = typeService.createType(request.getType());
        if (typeDto != null) {
            return ok(new TypeCreationResponse(typeDto.getType()));
        }
        return status(HttpStatus.UNAUTHORIZED).build();
    }
}
