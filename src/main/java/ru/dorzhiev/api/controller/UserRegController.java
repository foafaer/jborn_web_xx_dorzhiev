package ru.dorzhiev.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.dorzhiev.api.converter.UserRegConverter;
import ru.dorzhiev.api.json.RegRequest;
import ru.dorzhiev.api.json.RegResponse;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class UserRegController {
    private final UserService userService;
    private final UserRegConverter converter;

    @PostMapping("/registration")
    public ResponseEntity<RegResponse> signUp(@RequestBody @Valid RegRequest request) {
        UserDto userDto = userService.registration(
                request.getFirstName(),
                request.getLastName(),
                request.getEmail(),
                request.getPassword());
        if (userDto != null) {
            return ok(converter.convert(userDto));
        }
        return status(HttpStatus.UNAUTHORIZED).build();
    }
}
