package ru.dorzhiev.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.dorzhiev.api.json.AccListResponse;
import ru.dorzhiev.service.AccountDto;
import ru.dorzhiev.service.AccountService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

import java.util.Collections;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AccListController {
    private final AccountService accountService;
    private final UserService userService;

    @GetMapping("/accounts")
    public ResponseEntity<AccListResponse> getAccounts() {
        UserDto user = userService.currentUser();
        if (user == null) {
            return status(HttpStatus.UNAUTHORIZED).build();
        }
        List<AccountDto> accountDtoList = accountService.accountDtoList(user.getId());
        if (accountDtoList != null) {
            return ok(new AccListResponse(accountDtoList));
        }
        return ok(new AccListResponse(Collections.emptyList()));
    }
}
