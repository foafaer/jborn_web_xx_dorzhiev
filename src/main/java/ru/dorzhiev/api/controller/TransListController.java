package ru.dorzhiev.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.dorzhiev.api.json.TransListRequest;
import ru.dorzhiev.api.json.TransListResponse;
import ru.dorzhiev.service.TransactionDto;
import ru.dorzhiev.service.TransactionService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TransListController {
    private final TransactionService transactionService;
    private final UserService userService;

    @GetMapping("/transactions")
    public ResponseEntity<TransListResponse> getTransactions(@RequestBody @Valid TransListRequest request) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return status(HttpStatus.UNAUTHORIZED).build();
        }
        List<TransactionDto> transactionDtoList = transactionService.transactionDtoList(
                request.getType(),
                request.getStartTime(),
                request.getEndTime());
        if (transactionDtoList != null) {
            return ok(new TransListResponse(transactionDtoList));
        }
        return ok(new TransListResponse(Collections.emptyList()));
    }
}
