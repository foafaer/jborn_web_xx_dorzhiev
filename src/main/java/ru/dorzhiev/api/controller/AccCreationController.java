package ru.dorzhiev.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.dorzhiev.api.converter.AccCreationConverter;
import ru.dorzhiev.api.json.AccCreationRequest;
import ru.dorzhiev.api.json.AccCreationResponse;
import ru.dorzhiev.service.AccountDto;
import ru.dorzhiev.service.AccountService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AccCreationController {
    private final AccountService accountService;
    private final AccCreationConverter converter;
    private final UserService userService;

    @PostMapping("/create_account")
    public ResponseEntity<AccCreationResponse> createAccount(@RequestBody @Valid AccCreationRequest request) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        AccountDto accountDto = accountService.createAccount(
                request.getName(),
                request.getBalance(),
                user.getId());
        if (accountDto != null) {
            return ok(converter.convert(accountDto));
        }
        return status(HttpStatus.UNAUTHORIZED).build();
    }
}
