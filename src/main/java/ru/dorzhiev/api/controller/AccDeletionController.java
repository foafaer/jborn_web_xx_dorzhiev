package ru.dorzhiev.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.dorzhiev.api.json.AccDeletionRequest;
import ru.dorzhiev.api.json.AccDeletionResponse;
import ru.dorzhiev.service.AccountService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AccDeletionController {
    private final AccountService accountService;
    private final UserService userService;

    @PostMapping("/delete_account")
    public ResponseEntity<AccDeletionResponse> deleteAccount(@RequestBody @Valid AccDeletionRequest request) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return status(HttpStatus.UNAUTHORIZED).build();
        }
        accountService.deleteAccount(request.getAccountId());
        return status(HttpStatus.ACCEPTED).build();
    }
}
