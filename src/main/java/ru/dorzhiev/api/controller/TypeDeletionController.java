package ru.dorzhiev.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.dorzhiev.api.json.TypeDeletionRequest;
import ru.dorzhiev.api.json.TypeDeletionResponse;
import ru.dorzhiev.service.TypeService;
import ru.dorzhiev.service.UserDto;
import ru.dorzhiev.service.UserService;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TypeDeletionController {
    private final TypeService typeService;
    private final UserService userService;

    @PostMapping("/delete_type")
    public ResponseEntity<TypeDeletionResponse> deleteType(@RequestBody @Valid TypeDeletionRequest request) {
        UserDto user = userService.currentUser();
        if (user == null) {
            return status(HttpStatus.UNAUTHORIZED).build();
        }
        typeService.deleteType(request.getType());
        return status(HttpStatus.ACCEPTED).build();
    }
}
