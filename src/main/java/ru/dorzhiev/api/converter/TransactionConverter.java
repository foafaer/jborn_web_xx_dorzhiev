package ru.dorzhiev.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.dorzhiev.entity.Transaction;
import ru.dorzhiev.service.TransactionDto;

@Component
public class TransactionConverter implements Converter<Transaction, TransactionDto> {

    @Override
    public TransactionDto convert(Transaction transaction) {
        return new TransactionDto(
                transaction.getId(),
                transaction.getSum(),
                transaction.getTimeAndDate(),
                transaction.getFromAccount().getId(),
                transaction.getToAccount().getId());
    }
}
