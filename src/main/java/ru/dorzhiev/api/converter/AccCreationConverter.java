package ru.dorzhiev.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.dorzhiev.api.json.AccCreationResponse;
import ru.dorzhiev.service.AccountDto;

@Component
public class AccCreationConverter implements Converter<AccountDto, AccCreationResponse> {
    @Override
    public AccCreationResponse convert(AccountDto accountDto) {
        return new AccCreationResponse(
                accountDto.getId(),
                accountDto.getAccountName(),
                accountDto.getBalance(),
                accountDto.getUserId());
    }
}
