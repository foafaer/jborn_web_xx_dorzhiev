package ru.dorzhiev.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.dorzhiev.api.json.AuthResponse;
import ru.dorzhiev.service.UserDto;

@Component
public class UserAuthConverter implements Converter<UserDto, AuthResponse> {
    @Override
    public AuthResponse convert(UserDto userDto) {
        return new AuthResponse(
                userDto.getId(),
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getEmail());
    }
}
