package ru.dorzhiev.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.dorzhiev.api.json.RegResponse;
import ru.dorzhiev.service.UserDto;

@Component
public class UserRegConverter implements Converter<UserDto, RegResponse> {
    @Override
    public RegResponse convert(UserDto userDto) {
        return new RegResponse(
                userDto.getId(),
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getEmail());
    }
}
