package ru.dorzhiev.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.dorzhiev.api.json.TransCreationResponse;
import ru.dorzhiev.service.TransactionDto;

@Component
public class TransCreationConverter implements Converter<TransactionDto, TransCreationResponse> {
    @Override
    public TransCreationResponse convert(TransactionDto transactionDto) {
        return new TransCreationResponse(
                transactionDto.getId(),
                transactionDto.getSum(),
                transactionDto.getTimeAndDate(),
                transactionDto.getFromAccountId(),
                transactionDto.getToAccountId());
    }
}
