package ru.dorzhiev.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.dorzhiev.entity.Type;
import ru.dorzhiev.service.TypeDto;

@Component
public class TypeConverter implements Converter<Type, TypeDto> {

    @Override
    public TypeDto convert(Type type) {
        return new TypeDto(
                type.getId(),
                type.getType());
    }
}
