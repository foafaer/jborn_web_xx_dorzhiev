package ru.dorzhiev.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.dorzhiev.entity.Account;
import ru.dorzhiev.service.AccountDto;

@Component
public class AccountConverter implements Converter<Account, AccountDto> {
    @Override
    public AccountDto convert(Account account) {
        return new AccountDto(
                account.getId(),
                account.getAccountName(),
                account.getBalance(),
                account.getUser().getId());
    }
}
