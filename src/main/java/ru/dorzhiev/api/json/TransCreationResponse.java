package ru.dorzhiev.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class TransCreationResponse {
    private Long id;
    private BigDecimal value;
    private LocalDateTime dateTime;
    private Long fromAccId;
    private Long toAccId;
}
