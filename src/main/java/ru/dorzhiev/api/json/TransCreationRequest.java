package ru.dorzhiev.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransCreationRequest {
    @NotNull
    private Long fromAccId;
    @NotNull
    private Long toAccId;
    @NotNull
    private BigDecimal value;
    @NotNull
    private LocalDateTime dateTime;
    @NotNull
    private List<String> types;
}
