package ru.dorzhiev.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegResponse {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
}
