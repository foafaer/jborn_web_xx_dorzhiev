package ru.dorzhiev.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccCreationRequest {
    @NotNull
    private String name;
    @NotNull
    private BigDecimal balance;
}
