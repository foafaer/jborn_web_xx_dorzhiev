package ru.dorzhiev.api.json;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AccListRequest {
    @NotNull
    private Long userId;
}
