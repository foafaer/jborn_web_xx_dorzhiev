package ru.dorzhiev.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccCreationResponse {
    private Long id;
    private String name;
    private BigDecimal balance;
    private Long userId;
}
