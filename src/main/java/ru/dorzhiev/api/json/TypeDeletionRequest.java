package ru.dorzhiev.api.json;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class TypeDeletionRequest {
    @NotNull
    private String type;
}
