package ru.dorzhiev.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TypeCreationResponse {
    private String type;
}
