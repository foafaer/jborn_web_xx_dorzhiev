package ru.dorzhiev.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class TransListResponse {
    private List list;
}
